<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><?php print $head_title; ?></title>
<?php print $head ?>
<style type="text/css" media="all">@import "themes/occy/admin-layout.css";</style>
<style type="text/css" media="all">@import "themes/occy/admin-modules.css";</style>
<style type="text/css" media="all">@import "themes/occy/admin-style.css";</style>
</head>
<body>

<!-- Admin Page Title -->
<h1 class="adminTitle">&raquo; <a href="/"><?php print $site ?></a> &laquo; Administration</h1>
<!-- Admin Page Title -->

<!-- print messages and breadcrumbs -->
<?php if ($messages != ""): ?>
   <div id="message"><?php print $messages ?></div>
<?php endif; ?>
<?php if (trim(strip_tags($breadcrumb))) { print $breadcrumb; } ?>
<!-- print messages and breadcrumbs -->

<!-- Start Sidebar Left -->
        <div id="SidebarLeft">
                <?php print $sidebar_left ?>
        </div>
<!-- End Sidebar Left -->

<!-- Start Content -->
<div id="Center">
        <!-- Start newsItems -->
        <div class="newsItems">
          <?php if ($tabs != ""): ?>
            <div class="tabs"><?php print $tabs ?></div>
          <?php endif; ?>
        <?php print($content) ?>
        </div>
        <!-- End newsItems -->
</div>
<!-- End Content -->


</body>
</html>

