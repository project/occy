<?php if (arg(0) == 'admin' || (arg(0) == 'node' && (arg(1) == 'add' || arg(2) == 'edit' ))) {include ('page-admin.tpl.php');die();} ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><?php print $head_title; ?></title>
<?php print $head ?>
<?php print $styles ?>
<style type="text/css" media="all">@import "themes/occy/layout.css";</style>
<style type="text/css" media="all">@import "themes/occy/modules.css";</style>
</head>
<body>

<!-- Begin Primary Links -->
	<div class="primary-links">
<?php
        if (is_array($primary_links)) {
	  $count = count($primary_links);
	  $countval = 0;
          foreach ($primary_links as $link) {
	    $countval++;
                if ($countval != $count) {
			print "<span class=\"primary-border\">";
		} else {
			print "<span class=\"primary\">";
		}
                print $link;
		print "</span>";
          }
        }
?>
	</div> 
<!-- End primary-links -->


<!-- Start Title Logo -->
<div class="headerLogo">
        <?php if ($site_slogan) : ?>
        <a href="<?php print url() ?>" title="Index Page"><span class="headerLogoText"><?php print($site_slogan) ?></span></a>
        <?php endif;?>
</div>
<!-- End Title Logo -->


<!-- Strat Wrapper div for right bg -->
<div id="Wrapper">


<!-- Start Content -->
<div id="Center">


	<!-- Start newsItems -->
        <div class="newsItems">
          <?php if ($tabs != ""): ?>
            <div class="tabs"><?php print $tabs ?></div>
          <?php endif; ?>
        <?php print($content) ?>
        </div> 
	<!-- End newsItems -->

</div> 
<!-- End Content -->


<!-- Start Sidebars -->
<!-- Start Sidebar Left -->
<div id="SidebarLeft">
	<div id="ContentLeft">
		<?php print $sidebar_left ?>
	</div>
</div>
<!-- End Sidebar Left -->

<!-- Start Sidebar Right -->
<div id="SidebarRight">
	<?php print $sidebar_right ?>
</div>
<!-- End Sidebar Right -->
<!-- End Sidebars -->

</div>
<!-- End Wrapper -->


<!-- Begin Footer -->
<div id="Footer">
<?php
	$footer_message = str_replace(array('<a', '</a>') ,array('<div class="footer-border"><a', '</a></div>'), $footer_message);
	print preg_replace('/footer-border(((?!footer-border).)+)$/', 'footer-border-enditem\1', $footer_message);
?>
</div>
<!-- End Footer -->


<?php print $closure; ?>
</body>
</html>

