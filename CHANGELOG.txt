CHANGELOG

###
Changes to the occy theme: Tue Mar 29 19:48:17 EST 2005
- $site_slogan implemented
- admin clean-up.
- tons of little changes.

###
Changes to the occy theme: Tue Mar 29 09:03:24 EST 2005
- page-admin.tpl.php
	thanks to chx, the admin theme has been added

- admin-layout.css, admin-modules.css, and admin-style.css
	developed for chx

###
Changes to the occy theme: Mon Mar 21 13:11:44 MST 2005
- page.tpl.php
	chx fixed the footer issue. (thanks!)

###
Changes to the occy theme: Thu Mar 17 11:57:20 EST 2005
- modules.css
	fixed navigation block	
	
- style.css
	fixed .menu padding / margins

- $submitted by wasn't a bug. 

###
Changes to the occy theme: Tue Mar  8 21:41:59 EST 2005

- modules.css 
	cleaned out some of the nasty block specific
	stuff and made it: #SidebarRight .block

- style.css
	fixed the footer in IE6 so there isn't a horizontal
	scroll bar.

- other minor changes.
